# Demo 数据测试

数据包括：
- 链接（需等待跳转）
- 发布时间（有些文章的发布时间没有未明确标示，是预估数值。误差应该不会超过2小时）
- 标题
- 文章全文

## 新闻按国家分类

### jp (日本)

2019-06-07 08:00:00到2019-06-07 08:00:00之间，取前100条。

- 电视台[NHK](nhk.or.jp)
- ~~电视台[TBS](tbs.co.jp)~~ <!--非新闻类网站、更新频率低-->
- 报纸[朝日新闻](asahi.com)
- 报纸[読売新聞](yomiuri.co.jp)
- 报纸[毎日新聞](mainichi.jp)
- 报纸[日本経済新聞](nikkei)
- ~~通讯社[一般社団法人共同通信社](kyodonews.jp)~~ <!--地址无效-->

### ru (俄罗斯)

2019-06-06 00:00:00+0000取前100条。

- 门户[mail.ru](news.mail.ru)
- 搜索引擎[yandex.ru](news.yandex.ru)
- 在线报纸[Lenta.ru](lenta.ru)
- 电视、报纸、网络媒体集团公司[rbc.ru](rbc.ru)

### it（意大利）

- 电视[Sky TG24](https://tg24.sky.it)
- 在线报纸[ilpost](ilpost.it)[^1]
- 报纸[repubblica](repubblica.it)
- 报纸[Corriere della Sera](corriere.it)[^2]
- 报纸[Il Fatto Quotidiano](ilfattoquotidiano.it)

[^1]: [wikipedia](https://en.m.wikipedia.org/wiki/Il_Post)
[^2]: [wikipedia](https://en.m.wikipedia.org/wiki/Corriere_della_Sera)
